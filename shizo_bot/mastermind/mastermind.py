import string
from random import randint

import vk_api
from vk_api.bot_longpoll import VkBotEventType, VkBotLongPoll


class CrazyBot(object):
    GROUP_ID = 190995928
    ACCESS_TOKEN = 'e481ecb69243a2bf0d6dc27c0deff2f721b9d0aaf' \
                   '6f8e9a8b08296af84c22ab1fdef9deb3bb0a3b36c010'
    vk_version = vk_api.VkApi(token=ACCESS_TOKEN)
    longpoll = VkBotLongPoll(vk_version, GROUP_ID)
    vk = vk_version.get_api()
    phrases = {
        "да": 'ПИЗДА!',
        'нет': "Пидора ответ!",
        "точно": "Соси сочно!",
        "ут": "В жопу семеро ебут!",
        "как": "Жопой об косяк",
        "ты тоже": "На говно похоже"

    }

    exc_phrases = ["я пас", "мы пас"]

    def run_bot(self):
        for event in self.longpoll.listen():
            if (
                    event.type == VkBotEventType.MESSAGE_NEW
                    and event.object['message']['text']
            ):
                request_massage = event.object['message']

                if event.from_chat:
                    message = None
                    request_massage['text'] = request_massage['text'].lower()
                    request_massage['text'] = ''.join(
                        l for l in request_massage['text']
                        if l not in string.punctuation
                    )
                    for phrase in self.phrases:
                        if request_massage['text'].endswith(phrase):
                            message = self.phrases.get(phrase)
                        if message:
                            break

                    if not message:
                        if any(
                                i in request_massage['text']
                                for i in self.exc_phrases
                        ):
                            message = "Ты сегодня Кутакбас!"

                    if message:
                        self.send_messages(request_massage['peer_id'], message,)

    def send_messages(self, peer_id, message):
        self.vk.messages.send(
            random_id=randint(0, 10000),
            message=message,
            peer_id=peer_id,
        )


if __name__ == '__main__':
    bot = CrazyBot()
    bot.run_bot()
